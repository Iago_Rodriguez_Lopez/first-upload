from django.shortcuts import render_to_response
from Upload.form import FormImagen
from django.template import RequestContext
from Upload.models import ModeloImagen


def handle(request):
    if request.method == 'POST':
        img = FormImagen(request.POST, request.FILES)
        if img.is_valid():

            new_internal = ModeloImagen(photo=request.FILES['photo'])
            new_internal.save()
            return render_to_response('temp/image_page.html',
                                      {'imagen': '../media/images/' +
                                          new_internal.photo.name.split('/')[-1]},
                                      RequestContext(request))
    else:
        img = FormImagen()

    return render_to_response('temp/principal_page.html',
                              {'form': img},
                              RequestContext(request))
