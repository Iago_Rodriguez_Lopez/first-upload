from django import forms


class FormImagen(forms.Form):
    photo = forms.ImageField(label='Choose your image')
